import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Header as HeaderRNE, Icon } from '@rneui/themed';
import { SafeAreaProvider } from 'react-native-safe-area-context';

const Header = (props) => {
  const docsNavigate = () => {
    Linking.openURL(`https://reactnativeelements.com/docs/${props.view}`);
    console.log('Docs navigation triggered');
  };

  const playgroundNavigate = () => {
    Linking.openURL(`https://@rneui/themed.js.org/#/${props.view}`);
    console.log('Playground navigation triggered');
  };

  return (
    <SafeAreaProvider>
      <HeaderRNE
        leftComponent={{
          icon: 'menu',
          color: '#fff',
        }}
        rightComponent={
          <View style={styles.headerRight}>
            <TouchableOpacity onPress={docsNavigate}>
              <Icon name="description" color="white" />
            </TouchableOpacity>
            <TouchableOpacity
              style={{ marginLeft: 10 }}
              onPress={playgroundNavigate}
            >
              <Icon type="antdesign" name="rocket1" color="white" />
            </TouchableOpacity>
          </View>
        }
        centerComponent={{ text: props.title, style: styles.heading }}
      />
    </SafeAreaProvider>
  );
};

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Header title="Header" />
      </View>
      <View style={styles.content}>
        <Text style={styles.text}>CONTEÚDO</Text>
        {/* Conteúdo principal */}
      </View>
      <View style={styles.footer}>
        <Text style={styles.text}>FOOTER SE NECESSÁRIO</Text>
        {/* Componente Rodapé / Footer */}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  header: {
    height: 50,
  },
  content: {
    flex: 1,
    backgroundColor: 'lightgreen',
  },
  footer: {
    height: 50,
    backgroundColor: 'lightcoral',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  heading: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
  },
  headerRight: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 5,
  },
});
