import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Menu from './src/menu/menu';
import ContagemCaracteres from './src/pages/contagemCaracteres'
import ContagemCliques from './src/pages/contagemCliques'
import ListaTextos from './src/pages/listaTextos'
import IdiomasDiferentes from './src/pages/IdiomasDiferentes'
import PaginaVazia from './src/pages/paginaVazia'

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu} />
        <Stack.Screen name="ContagemCaracteres" component={ContagemCaracteres} /> 
        <Stack.Screen name="ContagemCliques" component={ContagemCliques} /> 
        <Stack.Screen name="ListaTextos" component={ListaTextos} /> 
        <Stack.Screen name="IdiomasDiferentes" component={IdiomasDiferentes} />
        <Stack.Screen name="PaginaVazia" component={PaginaVazia} /> 
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;